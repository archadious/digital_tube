# Time-stamp: <2023-04-08 11:31:32 daniel>

##
# Tools
cc := /usr/bin/g++
ccflags := --std=c++23 -Wfatal-errors -Werror -Wunused

##
# Predefine the list of source files
src =

##
# Add rules and modules
include rules.mk
include ./src/module.mk

##
# Local varaibles
target := tube
objs = $(addsuffix .o,$(src))
deps = $(addsuffix .d,$(src))

##
# Primary build target
master : $(target) $(deps)
	@echo Build Complete

##
# Executable target
$(target) : $(objs)
	@echo OBJS: $(objs)
	$(cc) $(ccflags) -o $@ $^

##
# Execute the program
run : $(target) ; ./$<

##
# debug by viewing built variables
.PHONY : dump
dump :
	@echo SRC:  $(src)
	@echo OBJS: $(objs)
	@echo DEPS: $(deps)

##
# Clean up the build and remove object files
.PHONY : clean
clean : ; rm -f $(objs) $(deps) $(target)
