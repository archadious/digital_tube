#ifndef __DIGITAL_TUBE_HH__
#define __DIGITAL_TUBE_HH__


// Headers
//-----------------------------------------------------------------------------
#include <map>  // std::map
#include <vector>  // std::vector
#include <array>  // std::array
#include <string_view>  // std::string_view
#include "ht16k33.hh"  // ic::ht16k33


// MACROS
//-----------------------------------------------------------------------------
#define NUM_OF_DIGITS 4


//-----------------------------------------------------------------------------
namespace led {

  class digital_tube : public ic::ht16k33 {

  public:

    //    enum class digit : uint8_t { zero = 0x0, one = 0x01, two = 0x02, three = 0x03 };
    enum class digit : uint8_t { zero = 0x0, one, two , three };

    digital_tube( );
    ~digital_tube( );

    void set( digit, char );
    void set( const std::string_view& );

  protected:

  private:

    static std::map< char, const std::vector< uint8_t > > map;
    static std::array< digit, NUM_OF_DIGITS > digit_array;

  };  // end digital_tube

};  // end NS led


#endif  // __DIGITAL_TUBE_HH__
