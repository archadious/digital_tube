#ifndef __I2C_HH__
#define __I2C_HH__

// Headers
//-----------------------------------------------------------------------------
#include <cstdint>  // uint8_t

//-----------------------------------------------------------------------------
namespace bus {


  //---------------------------------------------------------------------------
  class i2c {

  public:

    using addr_t = uint8_t;

    i2c( addr_t );
    ~i2c( );

    void write( const uint8_t );
    void write( const uint8_t, const uint8_t );

  protected:

  private:

    int handle;
    uint8_t addr;

  };  // end i2c


};  // end bus


#endif  // __I2C_HH__
