# Time-stamp: <2023-04-08 11:27:04 daniel>

##
# Module: src

src += \
  ./src/main \
  ./src/digital_tube \
  ./src/ht16k33 \
  ./src/i2c
