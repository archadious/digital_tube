
// Headers
//-----------------------------------------------------------------------------
#include "ht16k33.hh"  // ic::ht16k33

//-----------------------------------------------------------------------------
ic::ht16k33::ht16k33( uint8_t addr ) : bus::i2c( addr ) {

  clear( );
  write( 0x21 );
  on( );
  dim( dim_duty::nine );

};  // end constructor


//-----------------------------------------------------------------------------
ic::ht16k33::~ht16k33( ) { };  // end destructor


#define MAX_ROW 8
//-----------------------------------------------------------------------------
void ic::ht16k33::clear( ) {

  for ( uint8_t row = 0; row < MAX_ROW; ++row )
    write( row, 0x0 );

};  // end clear


//-----------------------------------------------------------------------------
void ic::ht16k33::on( blink_rate rate ) {

  write( 0x81 | static_cast< uint8_t >( rate ) );

};  // display


//-----------------------------------------------------------------------------
void ic::ht16k33::dim( dim_duty rate ) {

  write( 0xe0 | static_cast< uint8_t >( rate ) );

};  // end dim
