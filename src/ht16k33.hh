#ifndef __HT16K33_HH__
#define __HT16K33_HH__

// Headers
//-----------------------------------------------------------------------------
#include "i2c.hh"  // bus::i2c


//-----------------------------------------------------------------------------
namespace ic {


  //---------------------------------------------------------------------------
  class ht16k33 : public bus::i2c {

  public:

    enum struct blink_rate : uint8_t {
      none = 0x0, two_hz = 0x02,
      one_hz = 0x04, half_hz = 0x06
    };  // end blink_rate

    enum struct dim_duty : uint8_t {
      one = 0x01, two = 0x02, three = 0x03, four = 0x04,
      five = 0x05, six = 0x06, seven = 0x07, eight = 0x08,
      nine = 0x09, ten = 0x0a, eleven = 0x0b, twelve = 0x0c,
      thirteen = 0x0d, fourteen = 0x0e, fifteen = 0x0f
    };  // end dim

    ht16k33( uint8_t );
    ~ht16k33( );

    void clear( );
    void on( blink_rate = blink_rate::none );
    void off( ) { write( 0x80 ); };
    void dim( dim_duty = dim_duty::ten );

  protected:

  private:

  };  // end ht16k33


};  // end NC ic

#endif  // __HT16K33_HH__
