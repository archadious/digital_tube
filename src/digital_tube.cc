
//-----------------------------------------------------------------------------
#include "digital_tube.hh"  // led::digital_tube


// Map of supported characters
//-----------------------------------------------------------------------------
std::map< char, const std::vector< uint8_t > > led::digital_tube::map{

  { 0x0, { 0x0, 0x0 } },
  { ' ', { 0x0, 0x0 } },

  { '0', { 0x3f, 0x0c } },
  { '1', { 0x06, 0x04 } },
  { '2', { 0xdb } },
  { '3', { 0x8f } },
  { '4', { 0xe6 } },
  { '5', { 0x69, 0x20 } },
  { '6', { 0xfd } },
  { '7', { 0x07 } },
  { '8', { 0xff } },
  { '9', { 0xef } },

  { 'A', { 0xf7 } },
  { 'B', { 0x8f, 0x12 } },
  { 'C', { 0x39 } },
  { 'D', { 0x0f, 0x12 } },
  { 'E', { 0x79 } },
  { 'F', { 0x71 } },
  { 'G', { 0xbd } },
  { 'H', { 0xf6 } },
  { 'I', { 0x09, 0x12 } },
  { 'J', { 0x1e } },
  { 'K', { 0x70, 0x24 } },
  { 'L', { 0x38 } },
  { 'M', { 0x36, 0x05 } },
  { 'N', { 0x36, 0x21 } },
  { 'O', { 0x3f } },
  { 'P', { 0xf3 } },
  { 'Q', { 0x3f, 0x20 } },
  { 'R', { 0xf3, 0x20 } },
  { 'S', { 0xed } },
  { 'T', { 0x01, 0x12 } },
  { 'U', { 0x3e } },
  { 'V', { 0x30, 0x0c } },
  { 'W', { 0x36, 0x28 } },
  { 'X', { 0x0, 0x2d } },
  { 'Y', { 0xee } },
  { 'Z', { 0x09, 0x0c } },

  { 'a', { 0x58, 0x10 } },
  { 'b', { 0x78, 0x20 } },
  { 'c', { 0xd8 } },
  { 'd', { 0x8e, 0x08 } },
  { 'e', { 0x58, 0x08 } },
  { 'f', { 0xc0, 0x14 } },
  { 'g', { 0x8e, 0x04 } },
  { 'h', { 0x70, 0x10 } },
  { 'i', { 0x0, 0x10 } },
  { 'j', { 0x10, 0x0a } },
  { 'k', { 0x0, 0x36 } },
  { 'l', { 0x30 } },
  { 'm', { 0xd4, 0x10 } },
  { 'n', { 0x50, 0x10 } },
  { 'o', { 0xdc } },
  { 'p', { 0x70, 0x01 } },
  { 'q', { 0x86, 0x04 } },
  { 'r', { 0x50 } },
  { 's', { 0x88, 0x20 } },
  { 't', { 0x78 } },
  { 'u', { 0x1c } },
  { 'v', { 0x10, 0x08 } },
  { 'w', { 0x14, 0x28 } },
  { 'x', { 0x0, 0x2d  } },
  { 'y', { 0x8e, 0x02 } },
  { 'z', { 0x48, 0x08 } },

  { '*', { 0xc0, 0x3f } },
  { '<', { 0x0, 0x24 } },
  { '>', { 0x0, 0x09 } },
  { '+', { 0xc0, 0x12 } },
  { '.', { 0x0, 0x40 } }

  };  // end _map


//-----------------------------------------------------------------------------
std::array< led::digital_tube::digit, NUM_OF_DIGITS >
led::digital_tube:: digit_array{
  led::digital_tube::digit::zero,
  led::digital_tube::digit::one,
  led::digital_tube::digit::two,
  led::digital_tube::digit::three
};  // end digit_array

//-----------------------------------------------------------------------------
led::digital_tube::digital_tube( ) : ic::ht16k33( 0x70 ) {

};  // end constructor


//-----------------------------------------------------------------------------
led::digital_tube::~digital_tube() { };


// Display 'letter' at digit 'which_digit'
//-----------------------------------------------------------------------------
void led::digital_tube::set( digit which_digit , char letter ) {

  uint8_t index = static_cast< uint8_t >( which_digit ) * 2;
  const auto bits = map.find( letter );

  if ( bits == map.end() ) return;

  for ( auto pattern : bits->second ) {
    write( index, 0x0 );
    write( index++, pattern );
  };  // end for loop

};  // end set


// Display upto four symbols in 'line' starting at the first digit
//-----------------------------------------------------------------------------
void led::digital_tube::set( const std::string_view& line ) {

  auto digit = digit_array.cbegin();

  for ( auto letter : line ) {
    set( *digit, 0x0 );  // clear the digit
    set( *digit, letter );  // display the letter
    if ( ++digit == digit_array.cend() ) break;
  };  // end for loop

};  // end set
