

// Header Files
//-----------------------------------------------------------------------------
#include <stdexcept>  // std::runtime_error
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <cstdint>  // uint8_t, uint16_t
#include "i2c.hh"  // bus::i2c::i2c


//-----------------------------------------------------------------------------
bus::i2c::i2c( addr_t addr ) : handle{ 0 }, addr( addr ) {

  handle = open( "/dev/i2c-8", O_RDWR );
  if ( handle < 0 ) throw std::runtime_error( "Open i2c bus failed" );

 if ( ioctl( handle, I2C_SLAVE, addr ) < 0 )
   std::runtime_error( "IOCTL error" );

};  // end constructor


//-----------------------------------------------------------------------------
bus::i2c::~i2c() { };  // end destructor


#define NUM_ONE 1
//-----------------------------------------------------------------------------
void bus::i2c::write( const uint8_t byte ) {

  ::write( handle, &byte, NUM_ONE );

};  // end write byte


#define NUM_TWO 2
//-----------------------------------------------------------------------------
void bus::i2c::write( const uint8_t one, const uint8_t two ) {

  uint8_t byte[] = { one, two };
  ::write( handle, byte, NUM_TWO );

};  // end write word
