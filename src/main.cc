

// Headers
//-----------------------------------------------------------------------------
#include <unistd.h>  // usleep
#include "digital_tube.hh"  // led::digital_tube


//-----------------------------------------------------------------------------
void scroll( led::digital_tube& tube ) {

  const std::string_view line{ "    THIS IS A TEST THIS IS ONLY A TEST  IF THIS HAD BEEN A REAL CONSUMER ELECTRONIC DEVICE I WOULD MARKET IT AND MAKE A LOT OF MONEY    " };

  for ( size_t pos = 0; pos < line.size(); ++pos ) {
    tube.set( line.substr( pos, NUM_OF_DIGITS ) );
    usleep( 350000 );
  };  // end for loop

};  // end scroll


//-----------------------------------------------------------------------------
void blink( led::digital_tube& tube ) {

  auto fnct = [ & ]( const std::string_view& text, led::digital_tube::blink_rate rate ) {
    tube.set( text );
    tube.on( rate );
    usleep( 3000000 );
  };  // end fnct

  fnct( "TWO ", led::digital_tube::blink_rate::two_hz );
  fnct( "ONE ", led::digital_tube::blink_rate::one_hz );
  fnct( "HALF", led::digital_tube::blink_rate::half_hz );
  fnct( "NONE", led::digital_tube::blink_rate::none );

};  // end blink


//-----------------------------------------------------------------------------
void dim( led::digital_tube& tube ) {

  auto fnct = [ & ]( uint8_t iter ) {
    tube.dim( static_cast< led::digital_tube::dim_duty >( iter ) );
    usleep( 600000 );
  };  // end fnct

  tube.set( "DIM " );
  for ( uint8_t iter = 15; iter != 0; --iter ) fnct( iter );

  tube.set( "BRHT" );
  for ( uint8_t iter = 1; iter < 16; ++iter ) fnct( iter );

};  // end dim


//-----------------------------------------------------------------------------
int main( void ) {

  led::digital_tube tube;

  while( true ) {

    scroll( tube );
    blink( tube );
    dim( tube );

  };  // end while

  return 0;

};  // end main
