# Ditigal Tube

Software support for the 4 bit digital meter tube that can be found on Aliexpress.  This display module includes two double 14-segment display modules on a PCB with a VT16K33 on the backside.

![Image of the Digital Meter Tube](img/4.bit.led.module.jpg)

## Authors and acknowledgment
Developed by Archadious


## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.


## Project status
Development Continues